import ase
import ase.io
import ase.visualize
import os
import numpy as np


def load_cif(fname):
    """
    Import a CIF file and print its contents

    Parameters
    ----------
    fname : str
        path to cif file

    Returns
    -------
    unitcell : ase.atoms.Atoms
        unitcell object
    """
    unitcell = ase.io.read(fname)  # Read cif file by using ase module
    unitcell.filename = fname
    unitcell.L_param = unitcell.get_cell_lengths_and_angles()[:3]
    unitcell.L_angles = unitcell.get_cell_lengths_and_angles()[3:]

    N = len(unitcell)  # Number of atoms in the unit cell

    # Print out information of the unit cell
    print("{}".format(unitcell.symbols))
    print("===Space group===")
    print("#: {}, symbol: {}".format(unitcell.info['spacegroup'].no,
                                     unitcell.info['spacegroup'].symbol))
    print("===Lattice Parameters===")
    print("a: {}\nb: {}\nc: {}".format(*list(unitcell.L_param)))
    print("alpha: {}\nbeta: {}\ngamma: {}".format(*list(unitcell.L_angles)))

    print("===Atomic sites===")
    for i in range(N):
        atom = unitcell[i]
        print("{}, \t [{:.4f}\t{:.4f}\t{:.4f}]".format(atom.symbol,
                                                       *atom.position))
    return unitcell


def check_cif_file(path):
    """Print raw contents of CIF file"""
    with open(path, 'r') as f:
        text = f.read()
        print(text.strip())


def _cartesian_basis_real(cell):
    """
    Return real lattice basis vectors in a cartesian representation
    """
    a, b, c, alpha, beta, gamma = cell.get_cell_lengths_and_angles()
    alpha = alpha/180*np.pi
    beta = beta/180*np.pi
    gamma = gamma/180*np.pi
    c1 = c*np.cos(beta)
    c2 = (c*b*np.cos(alpha)-c*b*np.cos(beta)*np.cos(gamma))/(b*np.sin(gamma))
    c3 = np.sqrt(c**2-c1**2-c2**2)
    M = np.array([[a, b*np.cos(gamma), c1],
                  [0, b*np.sin(gamma), c2],
                  [0,               0, c3]])
    return M


def _cartesian_basis_reciprocal(cell):
    """
    Return reciprocal lattice basis vectors in a cartesian representation
    """
    M = _cartesian_basis_real(cell)
    a = M[:, 0]
    b = M[:, 1]
    c = M[:, 2]
    V = np.dot(a, np.cross(b, c))
    ar = np.cross(b, c)/V
    br = np.cross(c, a)/V
    cr = np.cross(a, b)/V
    Mr = np.vstack([ar, br, cr]).T
    return Mr


def _real_to_cartesian(realv, cell):
    """
    Return the cartesian representation of a real lattice vector
    """
    M = _cartesian_basis_real(cell)
    return np.dot(M, realv)


def _reciprocal_to_cartesian(recipv, cell):
    """
    Return the cartesian representation of a reciprocal lattice vector
    """
    M = _cartesian_basis_reciprocal(cell)
    return np.dot(M, recipv)


def get_transform_matrix(ZA, Up_D, cell):
    """
    Generate a rotation matrix to transform the super cell

    Parameters
    ----------
    ZA : array-like, 3 elements
        Zone axis that will be parallel to the direction coming out of the
        image. Expressed in real lattice basis [uvw].
    Up_D : array_like, 3 elements
        Lattice planes perpendicular to the image y-axis. Expressed in
        reciprocal lattice basis (hkl)
    cell : unit cell object

    Returns
    -------
    matrix : 3x3 array
        Rotates original supercell onto new coordinates
    """
    # Check if the zone axis and upward directions are perpendicular
    assert np.dot(ZA, Up_D) == 0, ("Invalid (hkl) Upward direction. "
                                   "Two vectors should be orthogonal.")

    # Vectors parrallel to image Z, Y and X directions.
    # Right hand convention with Z pointing out of the image
    za_cart = _real_to_cartesian(ZA, cell)
    upd_cart = _reciprocal_to_cartesian(Up_D, cell)
    transv = np.cross(za_cart, upd_cart)

    # Normalize vectors, as it's simply a rotation
    za_cart = za_cart/np.linalg.norm(za_cart)
    upd_cart = upd_cart/np.linalg.norm(upd_cart)
    transv = transv/np.linalg.norm(transv)

    # new unit vectors
    matrix = np.array([transv,
                       upd_cart,
                       za_cart])
    # Transformation matrix M
    # M*za_cart => 001
    # M*upd_cart => 010
    # M*transv => 100
    print("=====Transformation matrix======")
    print("{}".format(matrix))
    return matrix


def quick_plot(supercell):
    """Make a quick and dirty plot of a supercell with 3D scatter plot"""
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(supercell.positions[:, 0], supercell.positions[:, 1],
               supercell.positions[:, 2])
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Z")


def rotate_supercell(init_cell, M):
    """Apply the rotation transformation M to the supercell init_cell"""
    Cell = init_cell.copy()    # copy the cell
    Cell.positions = np.dot(M, init_cell.positions.T).T  # rotate the coords
    CoM = Cell.get_center_of_mass()   # Find the center of mass (CoM)
    Cell.positions[:] = Cell.positions[:] - CoM  # shift the origin
    return Cell


def cut_to_box(init_cell, Box):
    """
    Cut a supercell to within a certain Box size

    Parameters
    ----------
    init_cell : ase.atoms.Atoms
        Supercell that needs to be cropped
    Box : list or array, 3 elements
        [width (x), length (y), thickness (z)] of box

    Returns
    -------
    cell : ase.atoms.Atoms
        Cropped supercell
    final_box_size : [Volume, array of 3 elements]
    """
    Cell = init_cell.copy()  # copy the cell
    bwb = np.array(Box)
    
    x, y, z = Cell.positions.T
    indexes = np.arange(Cell.positions.shape[0])
    condition_out = ((abs(x) > bwb[0]/2) | (abs(y) > bwb[1]/2) | (abs(z) > bwb[2]/2))
    outliers = indexes[condition_out]
    # check the position atom by atom if they are outside of the box.
    del Cell[outliers]
    # Then, delete them all.

    # setting a new origin
    # First, searching for the lowest coordinate that will include all atoms
    new_origin = np.min(Cell.positions, axis=0)
    
    # Shift the whole cell by the vector of "new_origin"
    # to set the atom of "new_origin" as (0,0,0)
    Cell.positions[:] = Cell.positions[:] - new_origin

    # Searching for the far most coordinate so all atoms are in the box
    # to set the final box size.
    final_box_size = np.max(Cell.positions, axis=0)
    Cell.set_cell(final_box_size)
    print("=====Final box size=====")
    print("x: {}\ny: {}\nz: {}".format(*final_box_size))
    return Cell, final_box_size


def export_xyz(Cell, path, output_fname):
    """Export a cell to path/output_fname"""
    output_fn = os.path.splitext(output_fname)[0] + "_xyz.xyz"
    output_pn = os.path.join(path, output_fn)
    ase.io.write(output_pn, Cell)


def export_prism(Cell, input_path, output_fname, DW_dic):
    """
    Export a cell to input_path/output_fname. You should provide
    Debye-Waller factors
    """
    output_fn = os.path.splitext(output_fname)[0] + "_prismatic.xyz"
    output_pn = os.path.join(input_path, output_fn)

    with open(output_pn, mode='w', encoding='utf-8') as f:
        # First row: Print original cif file name
        f.write(str(output_fname) + "\n")
        # Second row: Box size
        f.write("{}\t{}\t{}".format(*list(
                Cell.get_cell_lengths_and_angles()[:3])))
        f.write('\n')
        Z = Cell.arrays['numbers']  # atomic number
        positions = Cell.arrays['positions']  # atomic position
        occ = 1.0  # Occupancy of the atom
        for i in range(len(Cell)):
            DW = DW_dic.get(Z[i], 0.08)
            f.write("{}\t{:.8f}\t{:.8f}\t{:.8f}\t{}\t{}\n".format(
                    Z[i], *positions[i], occ, DW))
        # Print "-1" at the end of the file
        f.write("-1")
