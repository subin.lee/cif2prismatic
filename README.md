# Script for convering cif file into an input file for prismatic STEM simulation
CIF2PRISMATIC is a python code which converts a cif file into an input file format for Prismatic STEM simulation. From a .cif unit cell file, you can generate a supercell and align it to certain crystallographic orientation. Works for any unit cell.

# Prerequisites
- ASE
	- Atomic Simulation Environment
	- You can install by "pip install ase"
	- more information: https://wiki.fysik.dtu.dk/ase/

- Jupyter

- numpy


# How do I use CIF2PRISMATIC
The code in Jupyter notebook is composed of 4 parts.

1. Read cif file
	- Put your cif file into ./cell_files.


2. Generation of the transformation matrix
	- To generate the transformation matrix, first you need to provide crystallographic orientations of your supercell, zone axis and upward direction. With given orientations, the transformation matrix will be generated which will be applied to your supercell later.
 

3. Produce a supercell
	1. Before apply the transformation matrix, we need to make a chunk of crystal which repeats the unit cell. The size of the chunk, (Nx, Ny and Nz) should be decided considering your final supercell size. 
	2. Then, now we apply the transformation matrix to the chunk.
	3. Trim the rotated supercell into the final size of the supercell. You should provide the final box size (final_box) in angstrom unit. 
	4. Check the final supercell by using ase.visualize library.

4. Export to xyz or prismatic format
	- You can export to both standard xyz format and prismatic format. If you want to check your supercell in other softwares, like VESTA or OVITO, try it with .xyz format. Input file for prismatic is also .xyz but it has different structure. 








